 # **1 NEARSOFT**

Sitio web:

[Nearsoft](https://nearsoft.com/)

Ubicación:

[Sierra Ventana 268, Lomas 3ra Secc, 78210 San Luis, S.L.P.](https://goo.gl/maps/7aE2dvvXWDMjns9h7)

Acerca de :

Nearsoft tiene que ver con el desarrollo de software. Nuestros equipos se autogestionan y entregan a tiempo. Tratamos nuestro trabajo como un oficio y aprendemos unos de otros. Constantemente subimos el listón

Servicios (lista de servicios ofrecidos)

Presencia :

[Facebook](https://www.facebook.com/Nearsoft/)

[Twitter](https://twitter.com/Nearsoft)

[Linkedin](https://www.linkedin.com/company/nearsoft/)

Ofertas laborales : 

[Nearsoft Jobs](https://nearsoft.com/join-us/)

Blog de Ingeniería :

[Nearsoft Blog](https://nearsoft.com/blog/)

Tecnologías :
- Java
- React
- Ruby
- Python
- Node
- DevOps
- Full Stack
- Test
- WordPress


# **2 iTEXICO**

Sitio web: 

[iTexico](https://www.itexico.com)

Ubicación: https://g.page/improving-guadalajara?share

Acerca de: 

Improving Nearshore es una unidad de negocios de servicios digitales, subsidiaria de Improving, que ayuda a empresas emergentes, en crecimiento, medianas y empresariales a innovar en sus negocios. Integramos las mejores prácticas de equipos ágiles en cómo necesita la innovación digital e implementamos las mejores prácticas de entrega de servicios empresariales con centros de innovación en México. 

Servicios: 

- UI / UX Design
- Software Develoment Services
- Quality Assurance
- Mobile Development
- Cloud Services
- Artificial Intelligence
- .NET Development
- Java Development
- Full Stack JavaScript Practice
- Operations and Development Team Work

 Presencia:

 Facebook: https://www.facebook.com/ImprovingMx
 YouTube: https://www.youtube.com/channel/UCuYem4j89E0HbApDJJnyDlw
 Twitter: https://twitter.com/ImprovingMx?s=20
 Linked in: https://www.linkedin.com/showcase/improving-nearshore

Ofertas laborales:

https://www.itexico.com/careers#OPEN_ROLES

Blog de Ingeniería:

https://www.itexico.com/blog

Tecnologías:

- AWS
- Microsoft Azure
- Google AI
- Amazon Machine Learning
- Adobe Illustrator
- Adobe Photoshop
- Adobe Xd
- Figma
- HTML
- Jenkins
- Jira Software
- git
- Xray
- Android Studio

Lenguajes de programación: 

- C#
- Java
- Kotlin
- JavaScript
- .Net
- Python
- PHP

Frameworks:
- Appium
- React
- Angular
- Robot Framework
- Xamarin 

# **3 MAGMALABS**

Logotipo (imagen)

Sitio web:

[MagmaLabs](https://www.magmalabs.io/)

Ubicación:

[Plaza San Miguel, Av. Constitución 2035, Local 11-20, Planta Alta, Santa Gertrudis, 28017 Colima, Col.](https://goo.gl/maps/37SyitZi4K9ucaJHA)

Acerca de:

Somos una agencia de desarrollo de software estadounidense con operaciones en México y otras partes de América Latina. Desde el inicio, nuestra misión ha sido impulsar la innovación mientras desarrollamos talento humano de clase mundial. Durante los últimos diez años, hemos trabajado en más de cien proyectos para clientes de todo el mundo y nos hemos ganado una reputación notable como especialistas en comercio electrónico. Obie Fernandez (uno de los expertos en Ruby on Rails más conocidos del mundo) se unió a nosotros como socio y consultor jefe en enero de 2020 cuando nos fusionamos con su firma Kickass Partners.

Servicios :

- Expertos en Ruby On Rails, React y React Native 
- Líderes mundiales en Solidus para comercio electrónico
- Diseño de productos de software 

Presencia :

[Instagram](https://www.instagram.com/magmalabs)

[Linkedin](https://www.linkedin.com/company/magmalabs)

[Facebook](https://www.facebook.com/magmalabsio)

[Twitter](https://twitter.com/weareMagmaLabs)

Ofertas laborales :

[MagmaLabs Jobs](https://magmalabs.bamboohr.com/jobs/)

Blog de Ingeniería :

[MagmaLabs Technical Blog](http://blog.magmalabs.io/)

Tecnologías :

- Ruby
- React 
- React Native 
- Solidus

# **4 GRUPO GFT**

Logotipo (imagen)

Sitio web :

[Grupo gft](https://www.gft.com/mx/es/index/)

Ubicación :

[Gobernador Agustín Vicente Eguia 46, San Miguel Chapultepec I Secc, Miguel Hidalgo, 11850 Ciudad de México, CDMX](https://goo.gl/maps/1AuLBQX7SW8hp1o5A)

Acerca de:

Comenzamos como una pequeña empresa en la Selva Negra alemana y hemos crecido hasta convertirnos en especialistas del sector financiero a nivel internacional. Somos pioneros en ofrecer servicios nearshore desde 2001 y contamos con un equipo de 7.000 colaboradores en más de 15 países.
Sin embargo,  lo más importante  es que seguimos siendo humanos. Somos fieles a nuestros principios porque son muy importantes para nosotros. Nos apasiona la tecnología y nos encanta descubrir cosas nuevas. Combinamos una praxis según las tendencias consolidadas del sector junto con nuevas formas de pensar, integrando ambos conceptos en una solución que te permita destacar frente a tus competidores.

Servicios :

- Ayudamos a los bancos a adaptarse a un mercado cambiante con nuevos participantes y normativas
- GFT acompaña a importantes aseguradoras en su transformación digital
- Permite mejorar la eficiencia, acelerar la agilidad, reducir costos, explotar nuevos modelos comerciales y aprovechar nuevas oportunidades de mercado.

Presencia :

[Youtube](https://www.youtube.com/user/gftgroup)

[Linkedin](https://www.linkedin.com/company/gft-group)

[Facebook](https://www.facebook.com/gft.mex)

[Twitter](https://twitter.com/gft_mx)

Ofertas laborales :

[Grupo gft Jobs](https://jobs.gft.com/Mexico/go/mexico/4412401/)

Blog de Ingeniería :

[GTF Blog](https://blog.gft.com/es/)

Tecnologías :

- INTELIGENCIA ARTIFICIAL
- BLOCKCHAIN Y DLT
- CLOUD
- ANÁLISIS DE DATOS
- GREENCODING

# **5 SOULTECH**

Sitio web :

[Soultech](http://www.soultech.com.mx/)

Ubicación :

[C. Versalles 21, Juárez, Cuauhtémoc, 06600 Ciudad de México, CDMX](https://goo.gl/maps/yKy2FxH2sSTm9uG59)

Acerca de :

En SoulTech creemos en el talento y las capacidades de los mexicanos. Por ello apoyamos las iniciativas de generación de nuevos talentos, persiguiendo el crecimiento continuo, en un ambiente de confianza, comunicación permanente y respeto; compartiendo conocimiento, experiencia e información.

Servicios :

- Servicios especializados de asesoría, acompañamiento y ejecución de actividades técnicas interdisciplinarias que se ejecutan en las oficinas del cliente o de manera remota
 - Centro de soporte especializado en plataformas Oracle y EMC, así como en la familia de productos de Oracle Fussion Middleware, para atender a clientes en idioma español o inglés
 - Industrialización de la fabricación de Software utilizando metodologías y procesos de producción altamente especializados. se puede ejecutar en las instalaciones del cliente o a distancia
 
Presencia :

[Facebook](https://www.facebook.com/pg/SoulTech-121555374541298/posts/?ref=page_internal)

[Twitter](https://twitter.com/soultechmx)

Ofertas laborales :

[Contacto directo con la empresa](contacto@soultech.com.mx)

Blog de Ingeniería :

[Diagnósticos](http://www.soultech.com.mx/diagn-sticos.html)

Tecnologías :

- Integración de soluciones complejas (SOA, BPM, BI, BD)
​- Fábrica de Software (Java, .Net, Php, Android/IOS)
- Enterprise Content Management (ECM)
- Desarollo de plantillas con xPression
- Oracle Fusion Middleware (OFMW)
# **6 TANGO**
Sitio web :

[Tango ](https://tango.io/)

Ubicación :

[Av La Paz 51, Sta Bárbara, 28079 Colima, Col.](https://goo.gl/maps/u5p3crJodUamonqW8)

Acerca de : 

Tango (anteriormente TangoSource), ayuda a empresas emergentes, medianas y empresariales innovadoras a desarrollar productos digitales impactantes a través de asociaciones apasionadas. ¡Hemos ayudado a más de 100 empresas a desarrollar y escalar sus productos de software desde 2009!

Servicios :
- Producto mínimo viable
- Aumento de personal
- Resolución de deuda técnica
- Estrategia de producto
- Escalabilidad y aceleración del producto

Presencia :

[Facebook](https://web.facebook.com/Tango.ioMX%5D)

[Linkedin](https://www.linkedin.com/company/tango-io/)

[Twitter](https://twitter.com/tango_io)

[Instagram](https://www.instagram.com/tangodotio/)

Ofertas laborales :

[Tango Jobs](https://jobs.lever.co/tango)

Blog de Ingeniería :

[Tango Blog](https://blog.tango.io/)

Tecnologías :
- DevOps
- Jenkins 
- Terraform 
- Estibador 
- Heroku
- WebDev
- JavaScript 
- HTML 
- CSS 
- Ruby o
- React nativo 
- Java 
- Kotlin 
- c
# **7 DIGITALONUS**

Sitio web :

[DigitalOnUs](https://www.digitalonus.com)

Ubicación :

[Blvd. Díaz Ordaz 127-Piso 19, Santa María, 64650 Monterrey, N.L.](https://goo.gl/maps/qfC9wLMMDpVrGe8LA)

Acerca de :

En DigitalOnUS, el término “Abierto” es sinónimo de la filosofía de Innovación Colectiva y Responsabilidad Colectiva. Desde los elementos de Diseño y la estructura de nuestra oficina física hasta la Comunicación y el Diálogo dentro y entre los equipos, en lo que creemos es en Apertura y Transparencia. También respaldamos firmemente y damos la bienvenida al talento diverso ya que enriquece las interacciones con los clientes, lo que se traduce en un gran valor agregado. . El diseño de nuestra oficina de espacio abierto, la plataforma de comunicación interna y el compromiso bidireccional continuo con los equipos y la administración abren el camino hacia un equipo empoderado, comprometido e inspirado.

Servicios :
- Desarrollo nativo en la nube
- Automatización de la nube híbrida
- Ingeniería de calidad
- DevSecOps (seguridad en la nube)
- Ingeniería del Caos y SRE
- Observabilidad

Presencia :

[Twitter](https://twitter.com/digitalonsocial)

[Facebook](https://www.facebook.com/digitalonus/)

[Linkedin](https://www.linkedin.com/company/digitalonus/)

[Youtube](https://www.youtube.com/channel/UC9BWG_si8LqECmGuY1IDedg)

Ofertas laborales :

[Contacto directo](https://www.digitalonus.com/contact-us/)

Blog de Ingeniería : 

[DigitalOnUs Blog](https://www.digitalonus.com/blog/)

Tecnologías :
- Sputnik
- Vaul Aide
- Consul Manager
- IAC Legos

# **8 AVIADA**
Sitio web :

[Aviada mx](https://aviada.mx/)

Ubicación :

[Bulevar Luis D. Colosio 671, Santa Fe, 83249 Hermosillo, Son.](https://goo.gl/maps/fvKY42ju7iXVkTyS8)

Acerca de :

Aviada es una empresa de nearshoring y offshoring en México para todas sus necesidades de aumento de personal. Nos especializamos en encontrar y cuidar a grandes empleados en los campos de desarrollo de software, medios digitales, diseño multimedia y otros

Servicios :
- HR
- Procurement
- Equipment Leasing
- Recruitment
- Legal Compliance
- Payroll Processing

Presencia :

[Facebook](https://www.facebook.com/aviadamx)

[Linkedin](https://www.linkedin.com/company/aviadamx/)

[Instagram](https://www.instagram.com/aviadamx/)

[Aviada](https://twitter.com/AviadaMX)

Ofertas laborales :

[Aviada Jobs](https://aviada.mx/trabaja-con-aviada/)

Blog de Ingeniería :

[Aviada Blog](https://aviada.mx/category/blog/)

Tecnologías :
- Python
- QA
- React
- React Native
- Swift
- Unity
- Wordpress
- Xamarin
- 3D Animation
- Accessibility (WCAG 2.0)
- Account Management
- Ad Ops / Trafficking
- Ad Bidding
- AMP
- Campaign Management
- Customer Service
- Graphic Design
# **9 SVITLA**
Sitio web :

[Svitla](https://svitla.com/)

Ubicación :

[C. Colonias #221-piso 4, Col Americana, Americana, 44160 Guadalajara,Jal.](https://goo.gl/maps/sHoHGjSpi6u1Wa8a6)

Acerca de :

Somos su conducto hacia las innovaciones tecnológicas más punteras. Ofrecemos un valor incomparable a nuestros clientes, que confían en nuestra experiencia y muchos años de experiencia en Managed Team Extension y AgileSquads.

Servicios :

- Extensión de equipo administrado
- AgileSquads
- Servicio de consultoría

Presencia :

[Youtube](https://www.youtube.com/channel/UC1nu2LV4_08GoZThHEindWA)

[Twitter](https://twitter.com/SvitlaSystemsIn)

[Facebook](https://www.facebook.com/SvitlaSystems)

[Instagram](https://www.instagram.com/svitlasystems/)

Ofertas laborales :

[Svitla Jobs](https://svitla.com/career)

Blog de Ingeniería :

[Svitla Blog](https://svitla.com/blog)

Tecnologías :
- CSS3 
- React 
- TypeScript
- Ruby
- Python
- QA
- DevOps
- HTML5, 
- JavaScript 
- React 

# **10 KATA SOFTWARE**

Sitio web :

[Kata software](https://kata-software.com/es/homeA)

Ubicación :

[Vito Alessio Robles 300, Florida, Álvaro Obregón, 01030 Ciudad de México, CDMX](https://goo.gl/maps/xoBmQcFWmRjF7dUm7)

Acerca de :

Kata Software nos ayuda a estar cerca de nuestros clientes sin importar dónde se encuentren. La tecnología móvil es sin duda un habilitador que nos ha permitido alcanzar nuestro objetivo: llevar servicios financieros a los sectores populares

Servicios :

- IA para generar análisis de datos
- Seguridad
- Soluciones SaaS
- Operación de productos financieros para sistemas de cobranza
- software contables
- sistemas de originación
- software de gestión financiera
 
Presencia :

[Facebook](https://www.facebook.com/Kata-Software-109450663980021/)

[Twitter](https://www.linkedin.com/company/37521547/)

Ofertas laborales [Kata Jobs](https://kata-software.com/es/trabaja-con-nosotros)

Blog de Ingeniería :

[Kata Blog](https://kata-software.com/es/publicaciones)

Tecnologías :

- Azure
- AWS
- Custom Software
- Gold Certified Microsoft Partner for Application Development
- Frameworks
- Kata Builder 
# **11 NOVA SOLUTIONS**

Sitio web :

[Nova Solutions](https://novasolutionsystems.com)

Ubicación :

[Eje 4 Sur 535-Piso 29, código 2, Del Valle Norte, Benito Juárez, 03103 Ciudad de México, CDMX](https://goo.gl/maps/VTza7VVKQrT385S76)

Acerca de :

Somos consultores competitivos técnicamente, con iniciativa, curiosidad y espíritu emprendedor. Nos adaptamos, colaboramos y respondemos a las necesidades del cliente, al entregar lo mejor de nosotros en cada proyecto.
Todo lo que aprendemos, lo mejoramos, reforzamos y lo compartimos. Nuestra base esencial es la comunicación

Servicios :

- Desarrollo de Chatbots
- Desarrollo de convertidor de API's
- Asignación de personal o vehículos
- Desarrollo de Paperless

Presencia :

[Facebook](https://www.facebook.com/novasolutionsystems/)

[Twitter](https://twitter.com/novasolutionsys/)

[Linkedin](https://www.linkedin.com/company/novasolutionsystems/)

Ofertas laborales :

[Nova Jobs](https://novasolutionsystems.com/unete/#vacantes)

Blog de Ingeniería :

[Nova Solutions](https://twitter.com/novasolutionsys/)

Tecnologías :

- Arduino
- git
- Jenkins
- Kubernetes
- Oracle
- PostgreSQL
- Microsoft MySQL Server
- Node.js
- MongoDB
- Redis

-Lenguajes de programación: 
C++
Java
JavaScript
TypeScript
Python


-Frameworks:
Mojito
Bootstrap
Angular
React
Spring
Vue

# **12 SERTI**

Sitio web :

[Serti mx](https://serti.com.mx/)

Ubicación :

[Shakespeare 6-piso 3, Verónica Anzúres, Miguel Hidalgo, 11590 Ciudad de México, CDMX](https://goo.gl/maps/X1RFKqeVZM5LyfWD8)

Acerca de :

Ofrecemos desarrollo de software basado en productos o servicios Web, utilizando ciclos de desarrollo “agiles” que incluyan un fuerte análisis, diseño, desarrollo, pruebas y en caso de necesitarlo mantenimiento, nos gusta desarrollar software web con un alto contenido de User-Experience, pantallas fáciles de utilizar esto ayudara no solo a tus usuarios si no al modelo de tu negocio

Servicios :

- Desarrollo de plataformas web
- Desarrollo gráfico
- Desarrollo de apps
- Reclutamiento IT

Presencia :

[Linkedin](https://www.linkedin.com/company/servicios-estrat-gicos-en-tecnolog-as-de-informaci-n/)

[Twitter](https://twitter.com/serTI_Mx)

[Facebook](https://www.facebook.com/SERTIMX/)

Ofertas laborales :

[Serti Jobs](https://serti.com.mx/unete/)

Blog de Ingeniería :

[Serti Blog](https://serti.com.mx/serti/)

Tecnologías :
- Java
- Spring Web
- .NET
- C++
- GlassFish
- Spring Batch
- Bootstrap
- Spring Security
- MySQL

# **13 INTELIMETRICA**

Sitio web :

[Intelimetrica](https://intelimetrica.com)

Ubicación :

[Calz. Gral. Mariano Escobedo 748, Anzures, Miguel Hidalgo, 11590 Ciudad de México, CDMX](https://goo.gl/maps/JBrZ2JdKUpyfsLgw7)

Acerca de :

Estamos dedicados y comprometidos a acelerar la innovación empresarial a través de tecnología, ingeniería y diseño superiores.

Servicios :
- UX Design
- Artificial Intelligence Deveploment
- ML
- Software Engineering

Presencia :

[Facebook](https://www.facebook.com/intelimetrica)

[Twitter](https://twitter.com/Intelimetrica)

[Linkedin](https://www.linkedin.com/company/intelimétrica/)

[Github](https://github.com/Intelimetrica)

Ofertas laborales :

[Intelimetrica Jobs](https://intelimetrica.com/careers)

Blog de Ingeniería :

[Intelimetrica Blog](https://www.facebook.com/intelimetrica/)

Tecnologías :
- AWS desirable
- REST API
- SOAP API
- GraphQI
- GRPC
- Git
- Kubernetes
- Docker
- MongoDB
- SQL
- Lenguajes de programación: 
- JavaScript
- Java
- C
- Matlab
- R
- Python


Frameworks:
- Flask
- FastApi
- Phoenix
 # **14 SIMPAT TECH**
 
Sitio web :

[Simpat](https://simpat.tech/)

Ubicación :

[Ave. Eugenio Garza Sada 3820 Torre Micrópolis, Piso 8, Más Palomas (Valle de Santiago), 64860 Monterrey, N.L](https://goo.gl/maps/nvX2gKpMJNCjJXaa9)

Acerca de :

Simpat es una empresa de consultoría de software personalizado con sede en Austin, Texas, con un centro de distribución de software de última generación en Monterrey, Mexico. Somos un equipo de profesionales de software innovadores que buscan alcanzar los desafíos más complejos y las metas de misión crítica de nuestros clientes


Servicios : 
- Software personalizado
- DevOps
- Soluciones Cloud
- Equipos mejorados
- Seguro de calidad
- Inteligencia de negocio

Presencia :

[Instagram](https://www.instagram.com/simpat.tech/)

[Facebook](https://www.facebook.com/simpat.tech)

[Linkedin](https://www.linkedin.com/company/simpattech/)

Ofertas laborales :

[Simpat Jobs](https://simpat.tech/careers/)

Blog de Ingeniería :

[Simpat Blog](https://simpat.tech/insights/)

Tecnologías :

- Azure
- AWS
- Kubernetes
- Docker
- PAaS
- RabbitMQ
- Microservices
-Cloud services
# **15 SCIO**

Sitio web :

[Scio mx](https://www.scio.com.mx)

Ubicación :

[Av Las Cañadas 501-Int. 230, Tres Marías, 58254 Morelia, Mich.](https://goo.gl/maps/uVGKnCZtrBgFPanQ8)

Acerca de :

Somos una consultora internacional de servicio completo que ha crecido de manera constante y rentable como proveedor de servicios a nuestros clientes en América y Europa.
Fundada en 2003, nuestra experiencia abarca muchas industrias con una variedad de clientes, desde emprendedores que inician nuevas empresas hasta compañías Fortune 500 que aumentan sus equipos subcontratando parte de su desarrollo.
Nuestro éxito nos ha permitido atraer un equipo altamente calificado y operar en un entorno con una excelente infraestructura. 

Servicios :

- Desarrollo de aplicaciones móviles
- Desarrollo de aplicaciones web
- Desarrollo de aplicaciones y servicios en la Nube
- Desarrollo de aplicaciones de negocios
- Consultoría de Software

Presencia :

[Facebook](https://www.facebook.com/ScioMx)

[Twitter](https://twitter.com/sciomx)

[Linkedin](https://mx.linkedin.com/company/scio-consulting)

Ofertas laborales :

[Scio Jobs](https://www.scio.com.mx/trabaja-scio-mexico/vacantes/)

Blog de Ingeniería : [Scio Blog](https://www.scio.com.mx/blog/)

Tecnologías :
- PostgreSQL
- HTML
- CSS
- Lenguajes de programación: 
- Go!
- TypeScript
- JavaScript
- Swift
- Python
- Ruby
# **16 TACIT KNOWLEDGE**

Sitio web :

[Tacit Knowledge](https://www.tacitknowledge.com/)

Ubicación :

[Av. de las Américas 1545-Piso 7, Providencia, 44630 Guadalajara, Jal.](https://goo.gl/maps/YPcvFFjxwvnw7ZF16)

Acerca de :

Tacit Knowledge desarrolla, integra y respalda software empresarial para marcas conocidas y minoristas. Ofrecemos soluciones en todos los puntos de contacto del consumidor, desde el dispositivo hasta la puerta. Conectamos todo el ecosistema del negocio de comercio digital de nuestros clientes. Creamos y apoyamos el sitio web de comercio electrónico y las tecnologías asociadas a través del suministro de cumplimiento (Pick & Pack, Kitting ...) hasta las herramientas de entrega, devoluciones y participación del cliente posteriores a la compra

Servicios :

- Comunidad de código abierto
- Consultoría Ecommerce
- Rendimiento Máximo
- Comercio SAP
- Ncommerce
- Consultoría de Software
- Comercio Digital
- Servicios Gestionados

Presencia :

[Linkedin](https://www.linkedin.com/company/tacit-knowledge)

[Twitter](https://twitter.com/tacitknowledge)

Ofertas laborales :

[Tacit Jobs](https://www.tacitknowledge.com/careers)

Blog de Ingeniería :

[Tacit Blog](https://www.tacitknowledge.com/news/)

Tecnologías :

- Cloud 
- DevOps
- AWS/GCP
- Azure
- MySQL, MSSQL, PostgreSQL 
- Ruby
- Python

# **17 Enroute**

Sitio web: 

[Enroute systems](https://enroutesystems.com)

Ubicación: 

[Puerta del Sol Nte. 209, Dinastía, 64639 Monterrey, N.L.](https://g.page/enroutesystems?share)

Acerca de:

 Somos una empresa de desafío implacable del Status Quo a través del uso de la tecnología haciendo lo que amamos y con un enfoque en hacer felices y exitosos a nuestros clientes.  

Servicios: 

- Data Engineering (Ingeniería de Datos)
- Software Development (Desarrollo de Software)
- Quality Engineering (Ingeniería de Calidad)

Presencia:

[Facebook](https://www.facebook.com/enroutesystems) https://www.facebook.com/enroutesystems
[Instagram](https://www.instagram.com/enroutesystems/)
[Linkedin]( https://www.linkedin.com/company/enroutesystems)


Ofertas laborales:

[Enroute Jobs](https://www.linkedin.com/jobs/enroute-jobs-worldwide?f_C=10291722&trk=top-card_top-card-primary-button-top-card-primary-cta&position=1&pageNum=0)

Blog de Ingeniería:

[Enroute Blog](https://www.instagram.com/enroutesystems/)

Tecnologías:
- Microsoft Azure
- AWS
- Web driverio
- Postman
- Node.js
- Jenkins
- Github Actions
- Cucumber
- SonarQube
- Kubernete
- Apache Spark
- Apache nifi
- Google Cloud

Lenguajes de programación: 
- Java
- Python
- Ruby
- Scala
- Swift

Frameworks:
- Angular
- Spring
- React
- Selenium

# **18 ONEPHASE**

Sitio web :

[Onephase](https://onephase.com/)

Ubicación :

[Av. Revolución 4020-Local 6, Colonial La Silla, 64860 Monterrey, N.L](https://goo.gl/maps/ej2wycaBh6a9cnd66)

Acerca de :

Somos una empresa de consultoría tecnológica, ofrecemos soluciones tecnológicas personalizadas basadas en un enfoque centrado en el cliente. En Onephase ayudamos a su empresa a navegar en su viaje hacia la Transformación Digital.

Servicios :

- Digital Consulting
- Software Development
- Software Quality Assurance & Testing
- E-commerce Solutions
- Nearshore Staffing
- UI/UX Design
- VR/AR Services
- Cloud Services
- Big Data

Presencia :

[Instagram](https://www.instagram.com/onephasellc/)

[Twitter](https://twitter.com/OnePhaseLLC)

[Facebook](https://www.facebook.com/OnephaseLLC/)

Ofertas laborales :


Blog de Ingeniería :

[Onephase blog](https://onephase.com/insight)

Tecnologías :

Front-end

- Vue
- React
- Angular
- Unity
- ASP
- Back-end
- Rails
- Laravel
- Django
- NodeJS
- .NET
- Mobile
- Ionic
- Xamarin
- React
- Android

# **19 TELEPRO**

Sitio web :

[Telepro](https://telepro.com.mx)

Ubicación :

[Calle Gob.García Conde 28, San Miguel Chapultepec I Secc, Miguel Hidalgo, 11850 Ciudad de México, CDMX](https://goo.gl/maps/22oxmNbSnavwt6Lc7)

Acerca de :

Somos una empresa 100% mexicana, con 25 años de presencia en el mercado de tecnología de información. Nuestros socios y colaboradores, cuentan con amplia experiencia en el sector financiero y tecnológico.

Servicios :

- Administración de Ciclo de Crédito
- Business Intelligence y Analytical Solutions

Presencia :

[Facebook](https://www.facebook.com/InfoTelepro)

[Linkedin](https://www.linkedin.com/company/servicios-telepro-s.a.-de-c.v./)

Ofertas laborales :

[Telepro Jobs](https://telepro.com.mx/nosotros.html)

Blog de Ingeniería :

Tecnologías :

- SQL Server
- API REST
- HTML
- CSS
- AJAX

Lenguajes de programación: 
- ASP.Net C#
- JavaScript

Frameworks:
- Bootsrap

# **20 AGILE THOUGHT**

Sitio web :

[Agilethought](https://agilethought.com/)

Ubicación :

[Bosques de Alisos 45B, Bosques de las Lomas, Cuajimalpa de Morelos, 05120 Ciudad de México, CDMX](https://goo.gl/maps/MztxaorBwmwgkDx56)

Acerca de :

AgileThought es un socio de software personalizado y de transformación digital de servicio completo en el que Fortune 1000 confía para el diseño, el desarrollo, el soporte y la orientación para acelerar la innovación y garantizar resultados excepcionales durante todo el ciclo de vida del producto.

Servicios :

- Gestión de productos
- Transformaciones organizacionales
- Capacitación y certificaciones
- Estrategia Digital
- Ingeniería de aplicaciones
- Arquitectura y migración de la nube
- Análisis de datos avanzado

Presencia :

[Facebook](https://www.facebook.com/AgileThought)

[Twitter](https://twitter.com/AgileThought)

[Linkedin](https://www.linkedin.com/company/agilethought/)

Ofertas laborales :

[Agilethought Jobs](https://agilethought.com/careers/)

Blog de Ingeniería :

[Agilethought Blog](https://agilethought.com/insights/?_sft_category=blogs#resources)

Tecnologías :

